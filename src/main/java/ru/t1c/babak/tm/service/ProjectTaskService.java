package ru.t1c.babak.tm.service;

import ru.t1c.babak.tm.api.repository.IProjectRepository;
import ru.t1c.babak.tm.api.repository.ITaskRepository;
import ru.t1c.babak.tm.api.service.IProjectTaskService;
import ru.t1c.babak.tm.model.Project;
import ru.t1c.babak.tm.model.Task;

import java.util.List;

public class ProjectTaskService implements IProjectTaskService {

    private final IProjectRepository projectRepository;

    private final ITaskRepository taskRepository;

    public ProjectTaskService(final IProjectRepository projectRepository, final ITaskRepository taskRepository) {
        this.projectRepository = projectRepository;
        this.taskRepository = taskRepository;
    }

    @Override
    public void bindTaskToProject(final String projectId, final String taskId) {
        if (projectId == null || projectId.isEmpty() || taskId == null || taskId.isEmpty()) return;
        final Task task = taskRepository.findOneById(taskId);
        if (task == null) return;
        final Project project = projectRepository.findOneById(projectId);
        if (project == null) return;
        task.setProjectId(projectId);
    }

    @Override
    public void unbindTaskFromProject(final String projectId, final String taskId) {
        if (projectId == null || projectId.isEmpty() || taskId == null || taskId.isEmpty()) return;
        final Task task = taskRepository.findOneById(taskId);
        if (task == null || !task.getProjectId().equals(projectId)) return;
        final Project project = projectRepository.findOneById(projectId);
        if (project == null) return;
        task.setProjectId(null);
    }

    @Override
    public void removeProjectById(final String projectId) {
        if (projectId == null || projectId.isEmpty()) return;
        final List<Task> tasks = taskRepository.findAllByProjectId(projectId);
        for (Task task : tasks) taskRepository.removeById(task.getId());
        projectRepository.removeById(projectId);
    }

}
