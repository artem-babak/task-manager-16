package ru.t1c.babak.tm.controller;

import ru.t1c.babak.tm.api.controller.IProjectController;
import ru.t1c.babak.tm.api.service.IProjectService;
import ru.t1c.babak.tm.api.service.IProjectTaskService;
import ru.t1c.babak.tm.enumerated.Sort;
import ru.t1c.babak.tm.enumerated.Status;
import ru.t1c.babak.tm.model.Project;
import ru.t1c.babak.tm.util.DateUtil;
import ru.t1c.babak.tm.util.TerminalUtil;

import java.util.Arrays;
import java.util.Date;
import java.util.List;

public class ProjectController implements IProjectController {

    private final IProjectService projectService;

    private final IProjectTaskService projectTaskService;

    public ProjectController(final IProjectService projectService, final IProjectTaskService projectTaskService) {
        this.projectService = projectService;
        this.projectTaskService = projectTaskService;
    }

    /**
     * Render all projects in default order.
     */
    private void renderAllProjects() {
        final List<Project> projects = projectService.findAll();
        renderProjects(projects);
    }

    private void renderProjects(final List<Project> projects) {
        for (int i = 0; i < projects.size(); ) {
            final Project project = projects.get(i++);
            if (project == null) continue;
            System.out.println("\t" + i + ". " + project);
        }
    }

    @Override
    public void showProjectList() {
        System.out.println("[PROJECT LIST]");
        System.out.println("\tENTER SORT:");
        System.out.println("\t" + Arrays.toString(Sort.values()));
        final String sortType = TerminalUtil.nextLine();
        Sort sort = Sort.toSort(sortType);
        final List<Project> projects = projectService.findAll(sort);
        renderProjects(projects);
    }

    @Override
    public void clearProjects() {
        System.out.println("[PROJECT CLEAR]");
        projectService.clear();
    }

    @Override
    public void createProject() {
        System.out.println("[PROJECT CREATE]");
        System.out.println("\tENTER NAME:");
        final String name = TerminalUtil.nextLine();
        System.out.println("\tENTER DESCRIPTION");
        final String description = TerminalUtil.nextLine();
        System.out.println("\tENTER BEGIN DATE");
        final Date dateBegin = TerminalUtil.nextDate();
        System.out.println("\tENTER END DATE");
        final Date dateEnd = TerminalUtil.nextDate();
        projectService.create(name, description, dateBegin, dateEnd);
    }

    private void showProject(final Project project) {
        System.out.println("\tID: " + project.getId());
        System.out.println("\tNAME: " + project.getName());
        System.out.println("\tDESCRIPTION: " + project.getDescription());
        final Status status = project.getStatus();
        if (status != null) System.out.println("\tSTATUS: " + status.getDisplayName());
        System.out.println("\tCREATED: " + DateUtil.toString(project.getCreated()));
        System.out.println("\tDATE BEGIN: " + DateUtil.toString(project.getDateBegin()));
    }

    @Override
    public void showProjectById() {
        System.out.println("[SHOW PROJECT BY ID]");
        System.out.println("\tENTER ID: ");
        final String id = TerminalUtil.nextLine();
        final Project project = projectService.findOneById(id);
        showProject(project);
    }

    @Override
    public void showProjectByIndex() {
        System.out.println("[SHOW PROJECT BY INDEX]");
        renderAllProjects();
        System.out.println("\tENTER INDEX: ");
        final Integer index = TerminalUtil.nextNumber() - 1;
        final Project project = projectService.findOneByIndex(index);
        showProject(project);
    }

    @Override
    public void updateProjectById() {
        System.out.println("[UPDATE PROJECT BY ID]");
        System.out.println("\tENTER ID:");
        final String id = TerminalUtil.nextLine();
        projectService.findOneById(id);
        System.out.println("\tENTER NAME:");
        final String name = TerminalUtil.nextLine();
        System.out.println("\tENTER DESCRIPTION");
        final String description = TerminalUtil.nextLine();
        projectService.updateOneById(id, name, description);
    }

    @Override
    public void updateProjectByIndex() {
        System.out.println("[UPDATE PROJECT BY INDEX]");
        renderAllProjects();
        System.out.println("\tENTER INDEX:");
        final Integer index = TerminalUtil.nextNumber() - 1;
        projectService.findOneByIndex(index);
        System.out.println("\tENTER NAME:");
        final String name = TerminalUtil.nextLine();
        System.out.println("\tENTER DESCRIPTION");
        final String description = TerminalUtil.nextLine();
        projectService.updateOneByIndex(index, name, description);
    }

    @Override
    public void removeProjectById() {
        System.out.println("[REMOVE PROJECT BY ID]");
        System.out.println("\tENTER ID: ");
        final String id = TerminalUtil.nextLine();
        projectService.findOneById(id);
        projectTaskService.removeProjectById(id);
    }

    @Override
    public void removeProjectByIndex() {
        System.out.println("[REMOVE PROJECT BY INDEX]");
        renderAllProjects();
        System.out.println("\tENTER INDEX: ");
        final Integer index = TerminalUtil.nextNumber() - 1;
        final Project project = projectService.findOneByIndex(index);
        projectTaskService.removeProjectById(project.getId());
    }

    @Override
    public void changeProjectStatusById() {
        System.out.println("[CHANGE PROJECT STATUS BY ID]");
        System.out.println("\tENTER ID: ");
        final String id = TerminalUtil.nextLine();
        projectService.findOneById(id);
        System.out.println("\tENTER STATUS:");
        System.out.println("\t\t" + Arrays.toString(Status.values()));
        final String statusValue = TerminalUtil.nextLine();
        final Status status = Status.toStatus(statusValue);
        projectService.changeProjectStatusById(id, status);
    }

    @Override
    public void changeProjectStatusByIndex() {
        System.out.println("[CHANGE PROJECT STATUS BY INDEX]");
        renderAllProjects();
        System.out.println("\tENTER INDEX: ");
        final Integer index = TerminalUtil.nextNumber() - 1;
        projectService.findOneByIndex(index);
        System.out.println("\tENTER STATUS:");
        System.out.println("\t\t" + Arrays.toString(Status.values()));
        final String statusValue = TerminalUtil.nextLine();
        final Status status = Status.toStatus(statusValue);
        projectService.changeProjectStatusByIndex(index, status);
    }

    @Override
    public void startProjectById() {
        System.out.println("[START PROJECT BY ID]");
        System.out.println("\tENTER ID: ");
        final String id = TerminalUtil.nextLine();
        projectService.changeProjectStatusById(id, Status.IN_PROGRESS);
    }

    @Override
    public void startProjectByIndex() {
        System.out.println("[START PROJECT BY INDEX]");
        renderAllProjects();
        System.out.println("\tENTER INDEX: ");
        final Integer index = TerminalUtil.nextNumber() - 1;
        projectService.changeProjectStatusByIndex(index, Status.IN_PROGRESS);
    }

    @Override
    public void completeProjectById() {
        System.out.println("[COMPLETE PROJECT BY ID]");
        System.out.println("\tENTER ID: ");
        final String id = TerminalUtil.nextLine();
        projectService.changeProjectStatusById(id, Status.COMPLETED);
    }

    @Override
    public void completeProjectByIndex() {
        System.out.println("[COMPLETE PROJECT BY INDEX]");
        renderAllProjects();
        System.out.println("\tENTER INDEX: ");
        final Integer index = TerminalUtil.nextNumber() - 1;
        projectService.changeProjectStatusByIndex(index, Status.COMPLETED);
    }

}
