package ru.t1c.babak.tm.controller;

import ru.t1c.babak.tm.Application;
import ru.t1c.babak.tm.api.controller.ICommandController;
import ru.t1c.babak.tm.api.service.ICommandService;
import ru.t1c.babak.tm.model.Command;
import ru.t1c.babak.tm.util.InformationData;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class CommandController implements ICommandController {

    private final ICommandService commandService;

    public CommandController(ICommandService commandService) {
        this.commandService = commandService;
    }

    @Override
    public void showSystemInfo() {
        final Runtime runtime = Runtime.getRuntime();
        final int availableProcessors = runtime.availableProcessors();
        System.out.println("Available processors (cores): " + availableProcessors);
        final long freeMemory = runtime.freeMemory();
        final InformationData freeMemoryData = new InformationData(freeMemory);
        System.out.println("Free memory: " + freeMemoryData);
        final long maxMemory = Runtime.getRuntime().maxMemory();
        final InformationData maxMemoryData = new InformationData(maxMemory);
        System.out.println("Maximum memory: " +
                (maxMemory == Long.MAX_VALUE ? "no limit" : maxMemoryData));
        final long totalMemory = runtime.totalMemory();
        final InformationData totalMemoryData = new InformationData(totalMemory);
        System.out.println("Total memory available to JVM: " + totalMemoryData);
        final long usedMemory = totalMemory - freeMemory;
        final InformationData usedMemoryData = new InformationData(usedMemory);
        System.out.println("Used memory in JVM: " + usedMemoryData);
    }

    @Override
    public void showAbout() {
        System.out.println("[ABOUT]");
        System.out.println("Name: Artem Babak");
        System.out.println("E-mail: ababak@t1-consulting.ru");
    }

    /**
     * Getting version from pom.xml ${project.version}
     */
    @Override
    public void showVersion() {
        System.out.println("[VERSION]");
        final Properties properties = new Properties();
        final InputStream inputStream = Application.class
                .getClassLoader()
                .getResourceAsStream("project.properties");
        try {
            properties.load(inputStream);
            System.out.println(properties.getProperty("version"));
            if (inputStream != null) inputStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void showCommands() {
        final Command[] commands = commandService.getTerminalCommands();
        for (final Command command : commands) {
            final String name = command.getName();
            if (name == null || name.isEmpty()) continue;
            System.out.println(name + " : " + command.getDescription());
        }
    }

    @Override
    public void showArguments() {
        final Command[] commands = commandService.getTerminalCommands();
        for (final Command command : commands) {
            final String argument = command.getArgument();
            if (argument == null || argument.isEmpty()) continue;
            System.out.println(argument + " : " + command.getDescription());
        }
    }

    @Override
    public void showHelp() {
        System.out.println("[HELP]");
        final Command[] commands = commandService.getTerminalCommands();
        for (final Command command : commands)
            System.out.println(command);
    }

}
