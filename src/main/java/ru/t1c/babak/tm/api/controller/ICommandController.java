package ru.t1c.babak.tm.api.controller;

public interface ICommandController {

    void showSystemInfo();

    void showAbout();

    void showVersion();

    void showCommands();

    void showArguments();

    void showHelp();

}
