package ru.t1c.babak.tm.api.repository;

import ru.t1c.babak.tm.model.Project;

import java.util.Comparator;
import java.util.List;

public interface IProjectRepository {

    List<Project> findAll();

    List<Project> findAll(Comparator comparator);

    Project findOneById(String id);

    Project findOneByIndex(Integer index);

    Project add(Project project);

    Project create(String name);

    Project create(String name, String description);

    Project remove(Project project);

    Project removeById(String id);

    Project removeByIndex(Integer index);

    int getSize();

    void clear();

}
