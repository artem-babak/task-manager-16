package ru.t1c.babak.tm.component;

import ru.t1c.babak.tm.api.controller.ICommandController;
import ru.t1c.babak.tm.api.controller.IProjectController;
import ru.t1c.babak.tm.api.controller.IProjectTaskController;
import ru.t1c.babak.tm.api.controller.ITaskController;
import ru.t1c.babak.tm.api.repository.ICommandRepository;
import ru.t1c.babak.tm.api.repository.IProjectRepository;
import ru.t1c.babak.tm.api.repository.ITaskRepository;
import ru.t1c.babak.tm.api.service.*;
import ru.t1c.babak.tm.constant.ArgumentConst;
import ru.t1c.babak.tm.constant.TerminalConst;
import ru.t1c.babak.tm.controller.CommandController;
import ru.t1c.babak.tm.controller.ProjectController;
import ru.t1c.babak.tm.controller.ProjectTaskController;
import ru.t1c.babak.tm.controller.TaskController;
import ru.t1c.babak.tm.enumerated.Status;
import ru.t1c.babak.tm.exception.system.ArgumentNotSupportedException;
import ru.t1c.babak.tm.exception.system.CommandNotSupportedException;
import ru.t1c.babak.tm.model.Project;
import ru.t1c.babak.tm.repository.CommandRepository;
import ru.t1c.babak.tm.repository.ProjectRepository;
import ru.t1c.babak.tm.repository.TaskRepository;
import ru.t1c.babak.tm.service.*;
import ru.t1c.babak.tm.util.DateUtil;
import ru.t1c.babak.tm.util.TerminalUtil;

public class Bootstrap {

    private final ICommandRepository commandRepository = new CommandRepository();

    private final ICommandService commandService = new CommandService(commandRepository);

    private final ICommandController commandController = new CommandController(commandService);

    private final ITaskRepository taskRepository = new TaskRepository();

    private final ITaskService taskService = new TaskService(taskRepository);

    private final ITaskController taskController = new TaskController(taskService);

    private final IProjectRepository projectRepository = new ProjectRepository();

    private final IProjectService projectService = new ProjectService(projectRepository);

    private final IProjectTaskService projectTaskService = new ProjectTaskService(projectRepository, taskRepository);

    private final IProjectController projectController = new ProjectController(projectService, projectTaskService);

    private final IProjectTaskController projectTaskController = new ProjectTaskController(projectTaskService);

    private final ILoggerService loggerService = new LoggerService();

    public void run(final String[] args) {
        if (runArgument(args)) System.exit(0);
        initData();
        //predefinedCommands();
        initLogger();
        while (true) {
            try {
                System.out.println("ENTER COMMAND: ");
                final String command = TerminalUtil.nextLine();
                runCommand(command);
                loggerService.command(command);
                System.out.println("[OK]");
            } catch (final Exception e) {
                loggerService.error(e);
                System.err.println("[FAIL]");
            }
        }
    }

    private void initLogger() {
        loggerService.info("** WELCOME TO TASK-MANAGER! **");
        Runtime.getRuntime().addShutdownHook(new Thread() {
            public void run() {
                loggerService.info("** TASK-MANAGER IS SHUTTING DOWN **");
            }
        });
    }

    private void initData() {
        taskService.create("3Demo_task-1", "first demo task");
        taskService.create("1DEMO_TASK-2", "second demo task");
        taskService.create("2demo_task-3");
        taskService.updateOneByIndex(2, "2demo_task-3.1", "new description");
        projectService.create("3proj-1", "first demo proj");
        projectService.create("1proj-2", "second demo proj");
        projectService.create("2proj-3");
        projectService.updateOneByIndex(2, "2proj-3.1", "new description");
        projectService.add(new Project("1-1proj", Status.IN_PROGRESS, DateUtil.toDate("04.10.2019")));
        projectService.add(new Project("1-2proj", Status.NOT_STARTED, DateUtil.toDate("05.03.2018")));
        projectService.add(new Project("1-3proj", Status.IN_PROGRESS, DateUtil.toDate("16.02.2020")));
        projectService.add(new Project("1-4proj", Status.COMPLETED, DateUtil.toDate("22.01.2021")));
    }

    private void predefinedCommands() {
        runCommand("version");
        runCommand("task-list");
        runCommand("project-list");
//        runCommand("project-show-by-index");
//        runCommand("task-show-by-index");
//        runCommand("task-show-by-index");
//        runCommand("task-show-by-index");
//        runCommand("task-add-to-project-by-id");
//        runCommand("task-add-to-project-by-id");
//        runCommand("task-list-by-project-id");
    }

    public boolean runArgument(final String[] args) {
        if (args == null || args.length == 0) return false;
        final String arg = args[0];
        runArgument(arg);
        return true;
    }

    public void runArgument(final String arg) {
        if (arg == null || arg.isEmpty()) throw new ArgumentNotSupportedException();
        switch (arg) {
            case ArgumentConst.ABOUT:
                commandController.showAbout();
                break;
            case ArgumentConst.VERSION:
                commandController.showVersion();
                break;
            case ArgumentConst.HELP:
                commandController.showHelp();
                break;
            case ArgumentConst.INFO:
                commandController.showSystemInfo();
                break;
            default:
                throw new ArgumentNotSupportedException(arg);
        }
    }

    public void runCommand(final String command) {
        if (command == null || command.isEmpty()) throw new CommandNotSupportedException();
        switch (command) {
            case TerminalConst.ARGUMENTS:
                commandController.showArguments();
                break;
            case TerminalConst.COMMANDS:
                commandController.showCommands();
                break;
            case TerminalConst.ABOUT:
                commandController.showAbout();
                break;
            case TerminalConst.VERSION:
                commandController.showVersion();
                break;
            case TerminalConst.HELP:
                commandController.showHelp();
                break;
            case TerminalConst.INFO:
                commandController.showSystemInfo();
                break;
            case TerminalConst.TASK_LIST:
                taskController.showTaskList();
                break;
            case TerminalConst.TASK_CREATE:
                taskController.createTask();
                break;
            case TerminalConst.TASK_CLEAR:
                taskController.clearTasks();
                break;
            case TerminalConst.TASK_SHOW_BY_ID:
                taskController.showTaskById();
                break;
            case TerminalConst.TASK_SHOW_BY_INDEX:
                taskController.showTaskByIndex();
                break;
            case TerminalConst.TASK_UPDATE_BY_ID:
                taskController.updateTaskById();
                break;
            case TerminalConst.TASK_UPDATE_BY_INDEX:
                taskController.updateTaskByIndex();
                break;
            case TerminalConst.TASK_REMOVE_BY_ID:
                taskController.removeTaskById();
                break;
            case TerminalConst.TASK_REMOVE_BY_INDEX:
                taskController.removeTaskByIndex();
                break;
            case TerminalConst.TASK_CHANGE_STATUS_BY_ID:
                taskController.changeTaskStatusById();
                break;
            case TerminalConst.TASK_CHANGE_STATUS_BY_INDEX:
                taskController.changeTaskStatusByIndex();
                break;
            case TerminalConst.TASK_START_BY_ID:
                taskController.startTaskById();
                break;
            case TerminalConst.TASK_START_BY_INDEX:
                taskController.startTaskByIndex();
                break;
            case TerminalConst.TASK_COMPLETE_BY_ID:
                taskController.completeTaskById();
                break;
            case TerminalConst.TASK_COMPLETE_BY_INDEX:
                taskController.completeTaskByIndex();
                break;
            case TerminalConst.TASK_LIST_BY_PROJECT_ID:
                taskController.showTaskListByProjectId();
                break;
            case TerminalConst.TASK_ADD_TO_PROJECT_BY_ID:
                projectTaskController.bindTaskToProject();
                break;
            case TerminalConst.TASK_REMOVE_FROM_PROJECT_BY_ID:
                projectTaskController.unbindTaskFromProject();
                break;
            case TerminalConst.PROJECT_LIST:
                projectController.showProjectList();
                break;
            case TerminalConst.PROJECT_CREATE:
                projectController.createProject();
                break;
            case TerminalConst.PROJECT_CLEAR:
                projectController.clearProjects();
                break;
            case TerminalConst.PROJECT_SHOW_BY_ID:
                projectController.showProjectById();
                break;
            case TerminalConst.PROJECT_SHOW_BY_INDEX:
                projectController.showProjectByIndex();
                break;
            case TerminalConst.PROJECT_UPDATE_BY_ID:
                projectController.updateProjectById();
                break;
            case TerminalConst.PROJECT_UPDATE_BY_INDEX:
                projectController.updateProjectByIndex();
                break;
            case TerminalConst.PROJECT_REMOVE_BY_ID:
                projectController.removeProjectById();
                break;
            case TerminalConst.PROJECT_REMOVE_BY_INDEX:
                projectController.removeProjectByIndex();
                break;
            case TerminalConst.PROJECT_CHANGE_STATUS_BY_ID:
                projectController.changeProjectStatusById();
                break;
            case TerminalConst.PROJECT_CHANGE_STATUS_BY_INDEX:
                projectController.changeProjectStatusByIndex();
                break;
            case TerminalConst.PROJECT_START_BY_ID:
                projectController.startProjectById();
                break;
            case TerminalConst.PROJECT_START_BY_INDEX:
                projectController.startProjectByIndex();
                break;
            case TerminalConst.PROJECT_COMPLETE_BY_ID:
                projectController.completeProjectById();
                break;
            case TerminalConst.PROJECT_COMPLETE_BY_INDEX:
                projectController.completeProjectByIndex();
                break;
            case TerminalConst.EXIT:
                close();
                break;
            default:
                throw new CommandNotSupportedException(command);
        }
    }

    public void close() {
        System.exit(0);
    }

}
